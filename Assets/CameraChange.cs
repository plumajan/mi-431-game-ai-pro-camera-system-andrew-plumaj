﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChange : MonoBehaviour
{
    public GameObject thridCam;
    public GameObject firstCam;
    public int camMode;


    void Update()
    {
        if (Input.GetButtonDown("Camera"))
        {
            if (camMode == 1)
            {
                camMode = 0;
            }
            else
            {
                camMode += 1;
            }
            StartCoroutine(CamChange());
        }
    }

    IEnumerator CamChange()
    {
        yield return new WaitForSeconds(0.01f);
        if (camMode == 0)
        {
            thridCam.SetActive(true);
            firstCam.SetActive(false);
        }
        if (camMode == 1)
        {
            firstCam.SetActive(true);
            thridCam.SetActive(false);
        }
    }
}
