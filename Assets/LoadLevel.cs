﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene("Third Person Camera with Orbit");
    }

    public void Track()
    {
        SceneManager.LoadScene("Tracking Camera");
    }

    public void Swap()
    {
        SceneManager.LoadScene("Swapping Between First and Third");
    }

    public void Rail()
    {
        SceneManager.LoadScene("Rail Camera");
    }
}
