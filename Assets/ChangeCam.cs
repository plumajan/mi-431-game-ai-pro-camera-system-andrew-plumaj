﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCam : MonoBehaviour
{
    public GameObject thridCam;
    public GameObject firstCam;
    public int camMode;

    // Start is called before the first frame update
    void Start()
    {
        if (camMode == 0)
        {
            thridCam.SetActive(true);
            firstCam.SetActive(false);
        }
        if (camMode == 1)
        {
            firstCam.SetActive(true);
            thridCam.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (camMode == 0)
        {
            thridCam.SetActive(false);
            firstCam.SetActive(true);
        }
        if (camMode == 1)
        {
            firstCam.SetActive(false);
            thridCam.SetActive(true);
        }
    }
}
